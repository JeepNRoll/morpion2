//
// Created by Jeep on 28/02/2018.
//

#ifndef MORPION_PARTIE_H
#define MORPION_PARTIE_H

#include <iostream>
using namespace std;

#include "Joueur.h"
#include "Morpion.h"

class Partie {

protected:
    Joueur *joueur1;
    Joueur *joueur2;
    Joueur joueurs[NB_JOUEURS];
    Morpion *plateau;

public:
    Partie();

    void go();

    virtual ~Partie();


};



#endif //MORPION_PARTIE_H
