//
// Created by Jeep on 28/02/2018.
//

#ifndef MORPION_MORPION_H
#define MORPION_MORPION_H

#include "Joueur.h"
//#include "Partie.h"

char const NB_CASE(9);

class Morpion {
private:
    char grille[NB_CASE];

public:
    Morpion();

    char verifPos(int);
    void afficherGrille() const ;
    bool estGagner(Joueur) const ;

    void jouerCoup(const Joueur &joueur, int position);
};


#endif //MORPION_MORPION_H
