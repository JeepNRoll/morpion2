//
// Created by Jeep on 28/02/2018.
//

#ifndef MORPION_JOUEUR_H
#define MORPION_JOUEUR_H

#include <iostream>
int const NB_JOUEURS(2);
using namespace std;

class Joueur {

protected:
    char formPion;
    string nom;
    int score;
public:
    int getScore();

    void setScore(int score);

    char getFormPion() const;

    const string &getNom() const;

    Joueur();

    Joueur(const string &nom , char formPion);

};


#endif //MORPION_JOUEUR_H
