# Morpion en C++

### I . Architecture
* Morpion
    * header
        * Joueur.h
        * Morpion.h
        * Partie.h
    * sources
        * Joueur.cpp
        * Morpion.cpp
        * Partie.cpp
    * main.cpp
    
II . Fonctionnement
--
Vous avez le choix entre jouer une **partie rapide** ou **non**.

Le mode **partie rapide** permet de ne pas choisir le nom des joueurs ni de choisir les pions.

Dans le mode de jeu non rapide:

1. Indiquez le nom du joueur 1.
2. Choisissez un pion entre X et O.
3. Indiquez le nom du joueur 2.
    
Une partie se joue en trois manches gagnantes.

    