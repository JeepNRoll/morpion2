//
// Created by Jeep on 28/02/2018.
//

#include <utility>

#include "../header/Joueur.h"



Joueur::Joueur() = default;

char Joueur::getFormPion() const {
    return formPion;
}

const string &Joueur::getNom() const {
    return nom;
}

Joueur::Joueur(const string &nom, char formPion) {
    this->nom = nom;
    this->formPion = formPion;
    this->score = 0;
}

int Joueur::getScore()
{
    return score;
}

void Joueur::setScore(int score)
{
    this->score = score;
}





