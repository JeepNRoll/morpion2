//
// Created by Jeep on 28/02/2018.
//

#include "../header/Partie.h"



Partie::Partie(){

}

Partie::~Partie() {
    delete(joueur2);
    delete(joueur1);
    delete(plateau);
}

void Partie::go() {

    char partieRapide;



    cout << "Partie Rapide O/N" << endl;
    cin >> partieRapide;

    switch (partieRapide){
        default: case 'O':
        case 'o':
            this->joueur1 = new Joueur("Joueur 1", 'X');
            this->joueur2 = new Joueur("Joueur 2", 'O');
            break;
        case 'N':
        case 'n':
            string nom;
            char pion;
            cout << "Entrez le nom du Joueur 1: ";
            cin >> nom;
            cout << "Choisir un pion : [X,O] : ";
            cin >> pion;
            this->joueur1 = new Joueur(nom, pion);
            cout << "Entrez le nom du Joueur 2: ";
            cin >> nom;

            switch (joueur1->getFormPion()){
                default: case 'X':
                    this->joueur2 = new Joueur(nom, 'O');
                    break;
                case 'O':
                    this->joueur2 = new Joueur(nom, 'X');
                    break;
            }

            break;

    }

    this->joueurs[0] = *joueur1;
    this->joueurs[1] = *joueur2;

    cout << joueur1->getNom()
         << "( "
         << joueur1->getFormPion()
         << " )"
         << " VS "
         << joueur2->getNom()
         << "( "
         << joueur2->getFormPion()
         << " ) "
         << endl ;


    cout << "Entrez un chiffre entre 1 et 9 pour placer votre pion dans la grille." << endl;
    cout << "+---+---+---+" << endl;
    for (int i = 0; i < 3; ++i) {
        cout << "| " << 1+3*i << " | " << 2+3*i << " | " << 3+3*i << " |" << endl;
        cout << "+---+---+---+" << endl;
    }


    while (this->joueurs[0].getScore() < 3 &&
            this->joueurs[1].getScore() < 3 ){
        int n_coups = 0;
        this->plateau = new Morpion();

        cout << "Initialisation de la grille." << endl;
        this->plateau->afficherGrille();

        bool isWin = false;

        while( !isWin && n_coups < 9 ){
            Joueur thisJoueur = this->joueurs[n_coups % 2];
            int pos;

            cout << "C'est à "<< thisJoueur.getNom() << " de jouer [1-9]." << endl;

            if(!(cin>>pos)){
                cout << "Saisie incorecte" << endl;
                cin.clear();
                cin.ignore(numeric_limits<streamsize>::max(), '\n');
            } else
            {

                if (plateau->verifPos(pos))
                {

                    plateau->jouerCoup(thisJoueur, pos);
                    plateau->afficherGrille();

                    if (plateau->estGagner(thisJoueur))
                    {
                        isWin = true;
                        int score = thisJoueur.getScore();

                        this->joueurs[n_coups % 2].setScore(score + 1);

                        cout << thisJoueur.getNom() << " à gagné cette manche. " << endl;

                        for (int i = 0; i < NB_JOUEURS; i++)
                        {
                            cout << this->joueurs[i].getNom() << " : " << this->joueurs[i].getScore() << endl;
                        }
                    }

                    n_coups++;

                    if (n_coups == 9 && !isWin)
                    {
                        cout << "Egalité" << endl;
                    }
                } else
                {
                    cout << "Cette case à déjà été jouée." << endl;
                }
            }
        }
    }
    for (auto &joueur : this->joueurs)
    {
        if(joueur.getScore() == 3){
            cout << joueur.getNom() << " à Gagner la Partie !!!";
        }
    }

}






