//
// Created by Jeep on 28/02/2018.
//

#include "../header/Morpion.h"



Morpion::Morpion() {
    for (int i = 0; i < NB_CASE; i++) {
        this->grille[i] = ' ';
    }
}

void Morpion::jouerCoup( const Joueur &joueur, int position ) {
    if(Morpion::verifPos(position)){

        this->grille[position-1] = joueur.getFormPion();
    }
}

char Morpion::verifPos(int position) {
    return this->grille[position-1] == ' ';
}


void Morpion::afficherGrille() const {
    cout << "+---+---+---+" << endl;
    for (int i = 0; i < 3; ++i) {
        cout << "| " << this->grille[i*3] << " | " << this->grille[i*3+1] << " | " << this->grille[i*3+2] << " |" << endl;
        cout << "+---+---+---+" << endl;
    }
}

bool Morpion::estGagner(Joueur joueur) const {
    bool fini = false;

    int coupsGagnants[8][3];

    coupsGagnants[0][0] = 0;
    coupsGagnants[0][1] = 1;
    coupsGagnants[0][2] = 2;
    coupsGagnants[1][0] = 3;
    coupsGagnants[1][1] = 4;
    coupsGagnants[1][2] = 5;
    coupsGagnants[2][0] = 6;
    coupsGagnants[2][1] = 7;
    coupsGagnants[2][2] = 8;
    coupsGagnants[3][0] = 0;
    coupsGagnants[3][1] = 3;
    coupsGagnants[3][2] = 6;
    coupsGagnants[4][0] = 1;
    coupsGagnants[4][1] = 4;
    coupsGagnants[4][2] = 7;
    coupsGagnants[5][0] = 2;
    coupsGagnants[5][1] = 5;
    coupsGagnants[5][2] = 8;
    coupsGagnants[6][0] = 0;
    coupsGagnants[6][1] = 4;
    coupsGagnants[6][2] = 8;
    coupsGagnants[7][0] = 2;
    coupsGagnants[7][1] = 4;
    coupsGagnants[7][2] = 6;


    for (int i = 0; i < 8; i++) {
        if(this->grille[coupsGagnants[i][0]] == joueur.getFormPion()
           && this->grille[coupsGagnants[i][1]] == joueur.getFormPion()
           && this->grille[coupsGagnants[i][2]] == joueur.getFormPion() ){
            cout << "FIN" << endl;
            fini = true;
        }
    }

    return fini;

}



